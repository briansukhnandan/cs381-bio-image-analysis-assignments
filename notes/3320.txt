- If we rotate the image, we may run into a problem where the pixels fall out the array range, or that pixels don't fall in an integer range, but rather as decimal values which can be problematic.

- If we do a case where we round pixel values, we will run into situations where there are points where no pixel maps to, or 2 pixels may try to map to the same point.

- Global transformation transforms the entire image, whereas local transformation sections off a piece of the image and transforms it.

- In 2D, there are 6 degrees of freedom.
