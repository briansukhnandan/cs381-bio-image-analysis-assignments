# This is the root ITK CMakeLists file.
cmake_minimum_required(VERSION 3.10.2 FATAL_ERROR)

# This project is designed to be built outside the Insight source tree.
project(HelloWorld)

# Find ITK.
find_package(ITK REQUIRED)
include(${ITK_USE_FILE})

add_executable(cs381_biomedical_BrianSukhnandan_hw2 cs381_biomedical_BrianSukhnandan_hw2.cxx )

target_link_libraries(cs381_biomedical_BrianSukhnandan_hw2 ${ITK_LIBRARIES})
