#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkGradientAnisotropicDiffusionImageFilter.h"
#include "itkN4BiasFieldCorrectionImageFilter.h"
#include "itkThresholdImageFilter.h"
#include "itkConnectedThresholdImageFilter.h"

/**
 * Brian Sukhnandan && Resfred Arthur
 * CS381 - Biomedical Image Analysis: HW#3
 * **/

int main(int argc, char* argv[]) {

  // Set up typedefs for pixel type. Use float to allocate 32-bits.
  typedef float InputPixelType;
  typedef float OutputPixelType;

  // Declare typedef for ImageType, and r+w for Image I/O.
  typedef itk::Image< float, 2 > ImageType;
  typedef itk::ImageFileReader<ImageType> ReaderType;
  typedef itk::ImageFileWriter<ImageType> WriterType;

  // Setting up image types
  typedef itk::Image<InputPixelType, 2> InputImageType;
  typedef itk::Image<OutputPixelType, 2> OutputImageType;

  // Setting up filters
  typedef itk::GradientAnisotropicDiffusionImageFilter<InputImageType, OutputImageType> DenoisingType;
  typedef itk::N4BiasFieldCorrectionImageFilter<InputImageType, ImageType, OutputImageType> BiasCorrectionType;

  // Setting up smart pointers for filters.
  DenoisingType::Pointer DenoisingFilter = DenoisingType::New();
  BiasCorrectionType::Pointer BiasFilter = BiasCorrectionType::New();

  ReaderType::Pointer image_reader = ReaderType::New();
  WriterType::Pointer image_gray_writer = WriterType::New();
  //WriterType::Pointer image_white_writer = WriterType::New();

  // Set files to read/write to/from for image_reader and image_writer.
  image_reader->SetFileName(argv[1]);
  image_gray_writer->SetFileName("floatImage.hdr");
  //image_white_writer->SetFileName("whiteImage.hdr");

  // Allows us to set iterations and fitting levels for our
  // N4BiasFieldCorrection filter.
  typename BiasCorrectionType::VariableSizeArrayType maxiter(4);
  maxiter.Fill(75);

  // Set up BiasFieldCorrection with different parameters.
  BiasFilter->SetInput(image_reader->GetOutput());
  BiasFilter->SetConvergenceThreshold(5.0);
  BiasFilter->SetNumberOfFittingLevels(4);
  BiasFilter->SetMaximumNumberOfIterations(maxiter);

  // Apply our BiasFieldCorrection filter.
  try {
    BiasFilter->Update();
  }
  catch (itk::ExceptionObject & exp) {
    std::cerr << exp << std::endl;
    return 1;
  }

  // Set up DenoisingFilter with different parameters.
  DenoisingFilter->SetInput( BiasFilter->GetOutput() );
  DenoisingFilter->SetNumberOfIterations(5);
  DenoisingFilter->SetTimeStep( 0.18 );
  DenoisingFilter->SetConductanceParameter(5.0);

  // Apply our AnisotropicDiffusionFilter.
  try {
    DenoisingFilter->Update();
  }
  catch (itk::ExceptionObject & exp) {
    std::cerr << exp << std::endl;
    return 1;
  }

  image_gray_writer->SetInput(DenoisingFilter->GetOutput());
  image_gray_writer->Update();

  // -------------------------------------------------------------------------- //
  // --------------------Normal thresholding (Non-segmentation)---------------- //
  // -------------------------------------------------------------------------- //
  // -------------------------------------------------------------------------- //

  std::cout << std::endl << "Please save 'floatImage.hdr' as a .PNG through Fiji (ImageJ)" << std::endl;
  std::cout << "and name it 'input.png'. After placing in bin/ directory, press ENTER to continue." << std::endl;

  std::cin.ignore();
  
  typedef unsigned char TInputPixelType;
  typedef unsigned char TOutputPixelType;

  typedef itk::Image< unsigned char, 2 > TImageType;
  typedef itk::ImageFileReader<TImageType> TReaderType;
  typedef itk::ImageFileWriter<TImageType> TWriterType;

  TReaderType::Pointer thresh_image_reader = TReaderType::New();
  TWriterType::Pointer thresh_image_writer = TWriterType::New();

  // At this point in the program we have an image with bias correction and denoising.
  // Now we have to process the image as an unsigned char imagetype.

  typedef itk::ThresholdImageFilter<TImageType> ThreshType;
  ThreshType::Pointer GrayThresholdingFilter = ThreshType::New();

  thresh_image_reader = itk::ImageFileReader<TImageType>::New();

  try {
    thresh_image_reader->SetFileName("input.png");
  }
  catch (itk::ExceptionObject & exp) {
    std::cerr << exp << std::endl;
    return 1;
  }

  thresh_image_writer->SetFileName("thresholded_grayImage.png");

  // Set up ThresholdingFilter with different parameters.
  GrayThresholdingFilter->SetInput(thresh_image_reader->GetOutput());
  GrayThresholdingFilter->SetOutsideValue(0);

  // Isolate gray tissue.
  GrayThresholdingFilter->ThresholdOutside( 40,100 );

  // Apply our Thresholding filter.
  try {
    GrayThresholdingFilter->Update();
  }
  catch (itk::ExceptionObject & exp) {
    std::cerr << exp << std::endl;
    return 1;
  }

  // Do the same for thresholding white matter.
  thresh_image_writer->SetInput( GrayThresholdingFilter->GetOutput() );
	thresh_image_writer->Update();

  ThreshType::Pointer WhiteThresholdingFilter = ThreshType::New();
  thresh_image_writer->SetFileName("thresholded_whiteImage.png");

  WhiteThresholdingFilter->SetInput(thresh_image_reader->GetOutput());
  WhiteThresholdingFilter->SetOutsideValue(0);

  WhiteThresholdingFilter->ThresholdAbove(110);
  WhiteThresholdingFilter->ThresholdBelow(145);

  // Apply our filter for white matter.
  try {
    WhiteThresholdingFilter->Update();
  }
  catch (itk::ExceptionObject & exp) {
    std::cerr << exp << std::endl;
    return 1;
  }

  thresh_image_writer->SetInput( WhiteThresholdingFilter->GetOutput() );
	thresh_image_writer->Update();

  // -------------------------------------------------------------------------- //
  // ------------Segmentation using ConnectedThreshold Filter------------------ //
  // -------------------------------------------------------------------------- //
  // -------------------------------------------------------------------------- //

  typedef itk::ConnectedThresholdImageFilter< TImageType, TImageType > ConnectedThresholdingFilter;
  ConnectedThresholdingFilter::Pointer connectedThreshold = ConnectedThresholdingFilter::New();
  
  // Segment white matter of image using ConnectedThresholding filter.
  TReaderType::Pointer seg_white_reader = TReaderType::New();
  TWriterType::Pointer seg_white_writer = TWriterType::New();
  seg_white_reader->SetFileName("input.png");
  seg_white_writer->SetFileName("seg_white_image.png");

  // Get seed index for a spot in the picture that is white.
  // Look on imageJ for coordinates.
  ImageType::IndexType white_index;

  // t1
  //white_index[0] = 200;
  //white_index[1] = 300;

  // t2
  white_index[0] = 173;
  white_index[1] = 187;

  // Attributes for connected Threshold filter for separating white matter.
  connectedThreshold->SetInput(seg_white_reader->GetOutput());

  // t1
  //connectedThreshold->SetLower(95);
  //connectedThreshold->SetUpper(170);

  // t2
  connectedThreshold->SetLower(105);
  connectedThreshold->SetUpper(220);

  connectedThreshold->SetReplaceValue(255);
  connectedThreshold->SetSeed(white_index);

  connectedThreshold->Update();

  seg_white_writer->SetInput(connectedThreshold->GetOutput());
  try {
    seg_white_writer->Update();
  }
  catch (itk::ExceptionObject & exp) {
    std::cerr << exp << std::endl;
    return 1;
  }

  // Create filter for segmenting gray matter.
  connectedThreshold = ConnectedThresholdingFilter::New();

  // Segment white matter of image using ConnectedThresholding filter.
  TReaderType::Pointer seg_gray_reader = TReaderType::New();
  TWriterType::Pointer seg_gray_writer = TWriterType::New();
  seg_gray_reader->SetFileName("input.png");
  seg_gray_writer->SetFileName("seg_gray_image.png");

  // Get seed index for a spot in the picture that's gray.
  ImageType::IndexType gray_index;

  // t1
  //gray_index[0] = 96;
  //gray_index[1] = 406;
  
  // t2
  gray_index[0] = 176;
  gray_index[1] = 224;

  // Attributes for connected Threshold filter for separating white matter.
  connectedThreshold->SetInput(seg_gray_reader->GetOutput());

  // t1
  //connectedThreshold->SetLower(50);
  //connectedThreshold->SetUpper(100);

  // t2
  connectedThreshold->SetLower(35);
  connectedThreshold->SetUpper(100);

  connectedThreshold->SetReplaceValue(255);
  connectedThreshold->SetSeed(gray_index);

  connectedThreshold->Update();

  seg_gray_writer->SetInput(connectedThreshold->GetOutput());
  try {
    seg_gray_writer->Update();
  }
  catch (itk::ExceptionObject & exp) {
    std::cerr << exp << std::endl;
    return 1;
  }

  // -------------------------------------------------------------------------- //
  // -------------------------------------------------------------------------- //
  // -------------------------------------------------------------------------- //
  // -------------------------------------------------------------------------- //

  std::cout << "All images saved to bin/ directory." << std::endl;
 
  return 0;

}
