/*=========================================================================                                                                                      
 *                                                                                                                                                               
 *  Copyright Insight Software Consortium                                                                                                                        
 *                                                                                                                                                               
 *  Licensed under the Apache License, Version 2.0 (the "License");                                                                                              
 *  you may not use this file except in compliance with the License.                                                                                             
 *  You may obtain a copy of the License at                                                                                                                      
 *                                                                                                                                                               
 *         http://www.apache.org/licenses/LICENSE-2.0.txt                                                                                                        
 *                                                                                                                                                               
 *  Unless required by applicable law or agreed to in writing, software                                                                                          
 *  distributed under the License is distributed on an "AS IS" BASIS,                                                                                            
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.                                                                                     
 *  See the License for the specific language governing permissions and                                                                                          
 *  limitations under the License.                                                                                                                               
 *                                                                                                                                                               
 *=========================================================================*/

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkN4BiasFieldCorrectionImageFilter.h"
#include "itkGradientAnisotropicDiffusionImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkStatisticsImageFilter.h"
#include "itkThresholdImageFilter.h"

#include <iostream>
#include <string>

/**
 * Brian Sukhnandan
 * CS381 - Biomedical Image Analysis: HW#1
 * 
 * Filters used:
 * GradientAnisotropicDiffusionImageFilter - Used as an alternative to the PatchBasedDenoisingImageFilter
 * 											 for denoising because of its ability to preserve edges.
 * 
 * N4BiasFieldCorrectionImageFilter - Used for bias correction because of Professor Tsai's recommendation.
 * 									  Also contained very well documented examples which other bias correction filters
 * 									  failed to provide.
 * 
 * RescaleIntensityImageFilter - Used as an alternative to the HistogramMatchingImageFilter because we were not provided a reference
 * 						  		 Histogram to use as a parameter for the filter declaration. This filter sets intensity values
 * 								 based on min/max values supplied by user.
 * 
 * Names of pointers used for image I/O:
 * bcf - Bias Correction Image Filtering
 * dn - Denoising Image Filtering
 * rf - Intensity Standardized Filtering.
 * 
 * How to compile and use:
 * After running cmake, put 2 images in the bin directory: "clean_image.png" and
 * "noisy_image.png", and then run. The only std output that is generated is the
 * computation of the Coefficient of Variance of both images, and the output from
 * the filters is stored in outputImage.hdr, which you can open up with Fiji.
 * 
 * **/

/** 
 * computeCoV() - Computes Coefficient of Variation by taking (sigma/mean)*100
 * **/

double computeCoV(itk::StatisticsImageFilter<itk::Image< float, 2 >>::Pointer filter, itk::Image<float, 2>::Pointer imagePtr) {

	filter->SetInput(imagePtr);
	filter->Update();

	// CoV = (std dev / mean) * 100.
	return (filter->GetSigma() / filter->GetMean()) * 100;
}

int main() {
	
	std::cout << "Type 'p' to Process image using Bias Correction, Denoising, and Intensity Standardization." << std::endl;
	std::cout << "This looks for an image named 'input.dcm' in the bin/ directory." << std::endl << std::endl;
	
	std::cout << "Type 't' to perform thresholding on White or Gray tissue." << std::endl;
	std::cout << "This looks for an image named 'input.png' in the bin/ directory." << std::endl << std::endl;
	char choice;
	std::cin >> choice;

	if (choice == 'p') {
		
		char choiceForCleanImage;
		bool processCleanImage = false;

		std::cout << std::endl <<  "Do you want to compute the CoV for the clean image in this program? You must have a file named 'clean_image.png' in the bin/ directory (y/n)" << std::endl;

		std::cin >> choiceForCleanImage;

		if (choiceForCleanImage == 'y' || choiceForCleanImage == 'Y') {
			
			processCleanImage = true;	

		}

		// Set up typedefs for pixel type. Use float to allocate 32-bits.
		typedef float InputPixelType;
		typedef float OutputPixelType;

		// Declare typedef for ImageType, and r+w for Image I/O.
		typedef itk::Image< float, 2 > ImageType;
		typedef itk::ImageFileReader<ImageType> ReaderType;
		typedef itk::ImageFileWriter<ImageType> WriterType;

		// Setting up image types
		typedef itk::Image<InputPixelType, 2> InputImageType;
		typedef itk::Image<OutputPixelType, 2> OutputImageType;

		// Image mask. Make it type float and a 2D structure.
		// This will only be used in the typedef N4BiasFieldCorrectionImageFilter.
		typedef itk::Image<float, 2> MaskType;

		// Declare Pointers for Image r+w, and filters.
		ReaderType::Pointer reader = ReaderType::New();
		WriterType::Pointer writer = WriterType::New();

		// One extra reader for clean image.
		ReaderType::Pointer cleanReader = ReaderType::New();

		// Declare a GradientAnisotropicDiffusion filter for Denoising. 
		// Unlike the PatchBasedDenoisingFilter, This will preserve edges.
		typedef itk::GradientAnisotropicDiffusionImageFilter<InputImageType, OutputImageType> DenoisingType;
		typedef itk::N4BiasFieldCorrectionImageFilter<InputImageType, MaskType, OutputImageType> BiasCorrectionType;
		typedef itk::RescaleIntensityImageFilter<InputImageType, OutputImageType> RescaleType;
		
		
		// Declare typedef fpr StatisticsImageFilter, for computing CoV.
		typedef itk::StatisticsImageFilter<ImageType> StatisticsImageFilterType;

		DenoisingType::Pointer DenoisingFilter = DenoisingType::New();
		BiasCorrectionType::Pointer BiasFilter = BiasCorrectionType::New();
		RescaleType::Pointer IntensityStandardizingFilter = RescaleType::New();
		StatisticsImageFilterType::Pointer statisticsImageFilter = StatisticsImageFilterType::New();

		// Set up reader and writer, as well as input/output files.
		// ImageReader takes a DICOM image, and produces an hdr image.
		reader->SetFileName("input.dcm");
		writer->SetFileName("outputImage.hdr");
	
		if (processCleanImage) {
			cleanReader->SetFileName("clean_image.png");
		}

		// Allows us to set iterations and fitting levels for our
		// N4BiasFieldCorrection filter.
		typename BiasCorrectionType::VariableSizeArrayType maxiter(4);
		maxiter.Fill(75);

		// Set up BiasFieldCorrection with different parameters.
		BiasFilter->SetInput(reader->GetOutput());
		BiasFilter->SetConvergenceThreshold(5.0);
		BiasFilter->SetNumberOfFittingLevels(4);
		BiasFilter->SetMaximumNumberOfIterations(maxiter);

		// Apply our BiasFieldCorrection filter.
		try {
			BiasFilter->Update();
		}
		catch (itk::ExceptionObject & exp) {
			std::cerr << exp << std::endl;
			return 1;
		}
		OutputImageType::Pointer bcf = BiasFilter->GetOutput();

		// Set up DenoisingFilter with different parameters.
		DenoisingFilter->SetInput( bcf );
		DenoisingFilter->SetNumberOfIterations(5);
		DenoisingFilter->SetTimeStep( 0.18 );
		DenoisingFilter->SetConductanceParameter(5.0);
		
		// Apply our AnisotropicDiffusionFilter.
		try {
			DenoisingFilter->Update();
		}
		catch (itk::ExceptionObject & exp) {
			std::cerr << exp << std::endl;
			return 1;
		}
		// Set a pointer to the output of our filter.
		OutputImageType::Pointer dn = DenoisingFilter->GetOutput();

		// Set our Intensity Standardizing filter values, as well as set
		// our input to the output of the denoising filter.
		IntensityStandardizingFilter->SetInput(dn);
		IntensityStandardizingFilter->SetOutputMinimum(0);
		IntensityStandardizingFilter->SetOutputMaximum(255);

		// Apply the filter.
		try {
			IntensityStandardizingFilter->Update();
		}
		catch (itk::ExceptionObject & exp) {
			std::cerr << exp << std::endl;
			return 1;
		}
		OutputImageType::Pointer rf = IntensityStandardizingFilter->GetOutput();

		// Set writer's input to the image with the blurred effect applied.
		writer->SetInput( rf );
		writer->Update();

		// Compute CoV of clean image.
		if (processCleanImage) {
			
			std::cout << "CoV of Clean Image: " << computeCoV(statisticsImageFilter, cleanReader->GetOutput()) << std::endl;

		}

		// Compute CoV of processed image.
		std::cout << "CoV of Processed Image: " << computeCoV(statisticsImageFilter, rf) << std::endl;

	}

	else if (choice == 't') {

		// Set up typedefs for pixel type. Use float to allocate 32-bits.
		typedef unsigned char InputPixelType;
		typedef unsigned char OutputPixelType;

		// Declare typedef for ImageType, and r+w for Image I/O.
		typedef itk::Image< unsigned char, 2 > ImageType;
		typedef itk::ImageFileReader<ImageType> ReaderType;
		typedef itk::ImageFileWriter<ImageType> WriterType;

		// Setting up image types
		typedef itk::Image<InputPixelType, 2> InputImageType;
		typedef itk::Image<OutputPixelType, 2> OutputImageType;

		// Declare Pointers for Image r+w, and filters.
		ReaderType::Pointer reader = ReaderType::New();
		WriterType::Pointer writer = WriterType::New();

		//typedef itk::ThresholdImageFilter<InputImageType, OutputImageType> ThreshType;
		typedef itk::ThresholdImageFilter<OutputImageType> ThreshType;
		ThreshType::Pointer ThresholdingFilter = ThreshType::New();

		// Set up reader and writer, as well as input/output files.
		reader->SetFileName("input.png");
		writer->SetFileName("outputImage.hdr");

		ThresholdingFilter->SetInput(reader->GetOutput());
		ThresholdingFilter->SetOutsideValue(0);

		std::cout << std::endl << "Do you want to isolate White or Gray tissue?" << std::endl;
		std::cout << "Type 'gray' for Gray, and 'white' for White." << std::endl;
		std::string color;
		std::cin >> color;

		if (color == "gray") ThresholdingFilter->ThresholdOutside( 40,100 );

		else if (color == "white") {
			ThresholdingFilter->ThresholdAbove(110);
			ThresholdingFilter->ThresholdBelow(165);
		}

		// Apply our filter.
		try {
			ThresholdingFilter->Update();
		}
		catch (itk::ExceptionObject & exp) {
			std::cerr << exp << std::endl;
			return 1;
		}
		OutputImageType::Pointer tf = ThresholdingFilter->GetOutput();

		// Set writer's input to the image with the blurred effect applied.
		writer->SetInput( tf );
		writer->Update();

	}

	std::cout << std::endl << "Output saved to: bin/outputImage.hdr" << std::endl;

	return 0;

}
